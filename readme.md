# CSS - Animation


## Overview of CSS Transitions and Transform
* #### CSS Transforms
    * **Basic**
        * Transforms change the shape and position of the affected content by modifying the coordinate space.
        * Transforms do not disrupt the normal document flow.
    * **Reference**     
        * [MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/transform?v=a)
        *   [CSS-Tricks](https://css-tricks.com/almanac/properties/t/transform/)
        * [<transform-function>](https://developer.mozilla.org/en-US/docs/Web/CSS/transform-function "MDN")
    * `transform` can use `px`, and can use `%`, but the `%` refers to the object (width, height,..) itself, not the container
    * Transforms have a stacking order
    * add `perspective` for 3D transformation, its length can be set in px or ems, represents the approximate distance of how far away you are from the transforming element/the parent element. The lower its value is, the more drastic effect it has
    * `translate3d(X-axis, Y-axis, Z-axis)`
* #### CSS Transitions
    * Transitions need a beginning and end state to transition between
    * we can set different transitions for different properties
    * Reference on [MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Transitions/Using_CSS_transitions)

## Understanding CSS Animation
* #### Basic
    * **2 steps to a CSS animation**
        * define the animation
        * assign it to (a) specific element(s)
    * The `@keyframes` Block
        * a list describes what should happen
        * use `from` and `to`, or percentages to specifies the beginning and the end
        * `transform: translateX(0)` is a way to say "stay exactly at where you are"
* #### Animation function
    * **Reference**
        * [MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Animations/Using_CSS_animations)
        * [CSS tricks](https://css-tricks.com/almanac/properties/a/animation/)
    * to add to an element, use `animation-name` property, remember to add `animation-duration`
    * `animation-timing-function`: effect
    * `animation-iteration-count`: how many times the animation executes
    * `animation-delay`: the time to wait b4 the animation executes
    * `animation-fill-mode`: tell the element what to do outside of animation
        * `none` - the default value
        * `forwards` - stay at the last keyframe
        * `backwards` - stay at the first keyframe
        * `both` - apply first keyframe before animation starts, and last keyframe after animation ends
